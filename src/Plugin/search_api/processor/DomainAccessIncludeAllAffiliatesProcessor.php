<?php

namespace Drupal\domain_access_search_api\Plugin\search_api\processor;

use Drupal\domain\Entity\Domain;
use Drupal\search_api\Processor\ProcessorPluginBase;

/**
 * Massages the domain access field to include all the affiliates if necessary.
 *
 * @see \Drupal\search_api\Plugin\search_api\processor\Property\RenderedItemProperty
 *
 * @SearchApiProcessor(
 *   id = "domain_access_search_all_affiliates_processor",
 *   label = @Translation("Apply domain access all affliliates to allowed domains property"),
 *   description = @Translation("Preprocess the content types to build the content type facets. It requires to index the All affiliates field as well"),
 *   stages = {
 *     "preprocess_index" = -20,
 *   },
 * )
 */
class DomainAccessIncludeAllAffiliatesProcessor extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function preprocessIndexItems(array $items) {
    $options = NULL;
    foreach ($items as $item) {
      $all_affiliates = FALSE;
      /** @var \Drupal\search_api\Item\Item $item */
      $all_affiliates_fields = $this->getFieldsHelper()
        ->filterForPropertyPath($item->getFields(), 'entity:node', 'field_domain_all_affiliates');
      if (count($all_affiliates_fields)) {
        $all_affiliates_field = reset($all_affiliates_fields);
        $all_affiliated_field_values = $all_affiliates_field->getValues();
        $all_affiliates = reset($all_affiliated_field_values);
      }

      if (!$all_affiliates) {
        continue;
      }

      $fields = $this->getFieldsHelper()
        ->filterForPropertyPath($item->getFields(), 'entity:node', 'field_domain_access');
      if (empty(count($fields))) {
        continue;
      }
      if (!$options) {
        $all_domains = Domain::loadMultiple();
        $options = array_keys($all_domains);
      }
      foreach ($fields as $field) {
        $field->setValues($options);
      }
    }
  }

}
